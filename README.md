# GD GK

## Initialisation

```shell
# build python virtual envirement
python -m venv ./venv
# If your have multiple python version,use specific python version to build virtual envirement
py --list # list all python version
py -3.11 -m venv ./venv

# Set Powershell Limited
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

# Activate virtual envirement
.\venv\Scripts\Activate.ps1

# update pip version
python -m pip install --upgrade pip --index-url=https://pypi.org/simple

# install require model
pip install --upgrade -r requirements.txt --index-url=https://pypi.org/simple

# run fastapi by uvicorn
uvicorn app.main:app --port 8000 --reload

# main.py
python main.py
```

## OCR Result

![alt text](image.png)

## Deploy

```shell
# build docker image
docker build -t gdgk .

# run docker image
docker run -d -it --name=gdgk \
--net="host" \
-v ./:/app/ \
-e PROXY="http://192.168.8.15:10809" \
-e PRINT_FLUSH=false \
-e DEBUG=false \
-e ASYNC_COUNT=10 \
gdgk

docker run -d -it --name=gdgk \
--net="host" \
-v ./:/app/ \
-e PRINT_FLUSH=false \
-e DEBUG=false \
-e ASYNC_COUNT=20 \
gdgk

docker logs gdgk -f

# enter docker container
docker exec -it gdgk /bin/bash

# backup
# 原机器
docker save gdgk | gzip > gdgk.tar.gz

# 目标机器
gunzip -c gdgk.tar.gz | docker load

gzip -d gdgk.tar | docker load
```

## Git

```shell
git config user.name "Sophia Kube"
git config user.email "drhyy@proton.me"
```

## uvloop

use uvloop before:

![alt text](image-1.png)
