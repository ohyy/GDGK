# custom exceptions for the project


class GDGKException(Exception):
    pass


class VerifyCodeError(GDGKException):
    pass


class NotRegister(GDGKException):
    pass
