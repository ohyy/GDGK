import httpx
import asyncio
import time
from loguru import logger
from PIL import Image
import io
from typing import Any
from pathlib import Path
from functools import partial, wraps
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from exceptions import VerifyCodeError, GDGKException, NotRegister

# import pytesseract
import ddddocr
import random
import os


from dotenv import load_dotenv

load_dotenv()

# custom module
from text import generate_id

# https://github.com/joblib/loky
# best third-party cross-platfrom library for `concurrent.futures.ProcessPoolExecutor`
# from loky import get_reusable_executor


TOTAL, VERIFYERROR, HTTPERROR, SUCCESS, DETECTED = 0, 0, 0, 0, 0


### Config ###
PROXY: str | None = os.environ.get("PROXY", None)
DEBUG: bool = os.environ.get("DEBUG", "").upper() == "TRUE"
MAXWORKERS: int = int(os.environ.get("MAXWORKERS", 0))
PRINT_FLUSH: bool = os.environ.get("PRINT_FLUSH", "").upper() == "TRUE"
ASYNC_COUNT: int = int(os.environ.get("ASYNC_COUNT", 16))
DISABLE_UVLOOP: bool = os.environ.get("DISABLE_UVLOOP", "").upper() == "TRUE"
### End Config ###

if DEBUG:
    import tracemalloc

    tracemalloc.start()

# GDGK Login required cookies!
# from http.cookiejar import CookieJar
# class NullCookieJar(CookieJar):
#     """A CookieJar that rejects all cookies."""

#     def extract_cookies(self, *_):
#         """For extracting and saving cookies.  This implementation does nothing"""
#         pass

#     def set_cookie(self, _):
#         """Normally for setting a cookie.  This implementation does nothing"""
#         pass


# pytesseract.pytesseract.tesseract_cmd = "D:/Tesseract-OCR/tesseract.exe"
ORC = ddddocr.DdddOcr(show_ad=False)


max_worker = MAXWORKERS or min(32, (os.cpu_count() or 1) + 4)
# logger.success(f"Max worker: {max_worker}")
ThreadPool = ThreadPoolExecutor(max_workers=max_worker)
# ThreadPool = get_reusable_executor(max_workers=5)
ROOTPATH = Path(__file__).resolve().parent
Path(ROOTPATH / "pics").mkdir(exist_ok=True)
PICPATH = Path(ROOTPATH / "pics")
logger.success(
    f"Config: Max worker: {max_worker} Proxy: {PROXY} Debug: {DEBUG} Print flush: {PRINT_FLUSH} Async count: {ASYNC_COUNT} Disable uvloop: {DISABLE_UVLOOP}"
)

# when run in ProcessPoolExecutor, need to avoid block the process.
if __name__ == "__main__":
    if DEBUG:
        pause = input("Press any key to continue...")

cookies = {
    # "HttpOnly": "null",
    # "BIGipServerBSyMYKPyvqGQklQjIXmtMg": "!Eaw+M/Gh2JBb6L97nG2Gl/AFyIOcTZycSXtk0ESOpUkxO+00SEw6J5L9w9ybbCL1M3adu66pSwZ3Zg==",
    # "JSESSIONID": "Bf8qAFrBVp_POE1giI_siFySWLnKY9vYKZN3TH1nGgxw2FFsHAut!1548827114!-920218046",
    # "AMP_fe4beb374f": "JTdCJTIyZGV2aWNlSWQlMjIlM0ElMjJjODM4MzFkNi1iYTkwLTQzMDEtODc5YS1mZTc5ZTFiNzA3NDQlMjIlMkMlMjJ1c2VySWQlMjIlM0ElMjJjMDFiYTY1MC0xZjZhLTQzMmUtOWMzZS1jZjAwZDRhNzY2NWElMjIlMkMlMjJzZXNzaW9uSWQlMjIlM0ExNzE0MzI5MzAxMzU0JTJDJTIyb3B0T3V0JTIyJTNBZmFsc2UlN0Q=",
}

HEADER = {
    "Accept": "application/json, text/plain, */*",
    "Accept-Language": "en-US,en;q=0.9",
    "Connection": "keep-alive",
    "Content-Type": "application/json;charset=UTF-8",
    "Origin": "https://pg.eeagd.edu.cn",
    "Referer": "https://pg.eeagd.edu.cn/ks/h5/index.html",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "User-Agent": "Mozilla/5.0 (Linux; U; Android 5.0.2; zh-cn; Redmi Note 3 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 XiaoMi/MiuiBrowser/8.8.7",
    "Cache-Control": "no-cache",
    "Pragma": "no-cache",
    "Expires": "0",
}


class to_async:

    def __init__(self, *, executor: ThreadPoolExecutor | ProcessPoolExecutor):
        if not isinstance(executor, (ThreadPoolExecutor, ProcessPoolExecutor)):
            raise TypeError(
                "executor must be ThreadPoolExecutor or ProcessPoolExecutor"
            )
        self.executor = executor

    def __call__(self, blocking):
        @wraps(blocking)
        async def wrapper(*args, **kwargs):
            loop = asyncio.get_event_loop()
            # partial: fixed some function arguments, return a new function
            func = partial(blocking, *args, **kwargs)
            return await loop.run_in_executor(self.executor, func)

        return wrapper


@to_async(executor=ThreadPool)
def identify_code(img: io.BytesIO, standard=127.5) -> str:
    # logger.info(
    #     f"Enhance image execute in thread pool. thread: {threading.current_thread().name}"
    # )
    im = Image.open(img)

    # enhance image
    # 灰度转换 convert to gray
    im: Image.Image = im.convert("L")
    # 二值化
    pixels: Any = im.load()
    for x in range(im.width):
        for y in range(im.height):
            if pixels[x, y] > standard:
                pixels[x, y] = 255
            else:
                pixels[x, y] = 0

    # google tesseract ocr
    # string = pytesseract.image_to_string(image_gray)
    # ddddocr
    string = ORC.classification(im)

    # remove special characters
    def remove_special_char(s: str) -> str:
        # if chinese character, return it.
        if "\u4e00" <= s <= "\u9fa5":
            return ""
        return "".join(x for x in s if x.isalnum())

    string = remove_special_char(string)
    if len(string) != 4:
        logger.warning(f"Identify code length not match 4: {string}")
        raise VerifyCodeError(f"Identify code length not match 4: {string}")

    if string == "":
        logger.warning("Identify code is empty!")
        raise VerifyCodeError("Identify code is empty!")

    # im.save(Path(PICPATH / f"{string}.png").as_posix())

    return string


class CustomHTTPXAClient(httpx.AsyncClient):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def get(self, *args, **kwargs):
        self.cookies.clear()
        result = await super().get(*args, **kwargs)
        self.cookies.clear()
        return result

    async def post(self, *args, **kwargs):
        self.cookies.clear()
        result = await super().post(*args, **kwargs)
        self.cookies.clear()
        return result


class GDGK(object):

    def __init__(self) -> None:
        self.aClient = CustomHTTPXAClient(
            proxies=PROXY,
            headers=HEADER,
            transport=httpx.AsyncHTTPTransport(retries=3),
            limits=httpx.Limits(max_keepalive_connections=200, max_connections=200),
            verify=False,
            # cookies=NullCookieJar(),
        )
        self.pubkey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvJz/jW6ey4TLBRcDYTKsVuJHp7PWJvBtTFl3E5D0GrLOWM3JqfbHN0vHWhPKYzzQ53KI+E+TrQeXWGJh1SKd7DIL2sv/4mgX7PXtmtPLrdWDjHVCyl/mbTzenHN2vENi3byeBaiElTm4iEQf0rpVSczRJk0yz6lec86f0r1t2vwIDAQAB"
        self.cjsjs = []

    def getTimestamp(self):
        return int(time.time() * 1000)

    def ras_encrypt(self, data: str, pubkey: str) -> str:
        raise NotImplementedError

    async def clear_cjsjs(self):
        while True:
            # print("sleep 10s")
            await asyncio.sleep(10)
            # print("Clear cjsjs")
            self.cjsjs.clear()

    async def gen_verifycode_param(self) -> tuple[int, str, httpx.Cookies]:
        """
        will set a cookie in order to get the verify code picture.
        """
        # data = [1714397794710]
        data = [self.getTimestamp()]
        response = await self.aClient.post(
            "https://pg.eeagd.edu.cn/ks/auth/genAddcode.jsmeb",
            json=data,
        )
        res_json = response.json()
        cjsj = int(res_json["result"]["result"]["cjsj"])

        pubkey = str(res_json["result"]["result"]["pubkey"])
        if cjsj in self.cjsjs:
            logger.warning(f"Duplicate cjsj: {cjsj}, retry..")
            await asyncio.sleep(random.random())
            raise VerifyCodeError(f"Duplicate cjsj: {cjsj}")

        self.cjsjs.append(cjsj)
        return cjsj, pubkey, response.cookies

    async def get_verifycode_pic(
        self, cookies: httpx.Cookies
    ) -> tuple[io.BytesIO, httpx.Cookies]:
        resp = await self.aClient.get(
            url=f"https://pg.eeagd.edu.cn/ks/login/login.addcode", cookies=cookies
        )
        pic: io.BytesIO = io.BytesIO(resp.content)
        return pic, resp.cookies

    @to_async(executor=ThreadPool)
    def identify_code(self, img: io.BytesIO, standard=127.5) -> str:
        # logger.info(
        #     f"Enhance image execute in thread pool. thread: {threading.current_thread().name}"
        # )
        im = Image.open(img)

        # enhance image
        # 灰度转换 convert to gray
        im: Image.Image = im.convert("L")
        # 二值化
        pixels: Any = im.load()
        for x in range(im.width):
            for y in range(im.height):
                if pixels[x, y] > standard:
                    pixels[x, y] = 255
                else:
                    pixels[x, y] = 0

        # google tesseract ocr
        # string = pytesseract.image_to_string(image_gray)
        # ddddocr
        string = ORC.classification(im)

        # remove special characters
        def remove_special_char(s: str) -> str:
            # if chinese character, return it.
            if "\u4e00" <= s <= "\u9fa5":
                return ""
            return "".join(x for x in s if x.isalnum())

        string = remove_special_char(string)
        if len(string) != 4:
            logger.warning(f"Identify code length not match 4: {string}")
            raise VerifyCodeError(f"Identify code length not match 4: {string}")

        if string == "":
            logger.warning("Identify code is empty!")
            raise VerifyCodeError("Identify code is empty!")

        # im.save(Path(PICPATH / f"{string}.png").as_posix())

        return string

    @to_async(executor=ThreadPool)
    def save_pic(self, pic: io.BytesIO, name: str):
        img = Image.open(pic)
        img.save(Path(PICPATH / f"{name}.png"))

    async def login(
        self, username: str, addcode: str, cjsj: int, cookies: httpx.Cookies
    ):
        login_data = [
            {
                "username": f"{username}",
                "password": "UFuc+D+FwjfKVXus82FO33PecEx37uetXFW2wtj4aJEgUMnnXJUXGyQj9MXnxYgA7I7cRvodnlUENjl/YHNrUPg/dfaa1chffBd9+eJEpLnqnLpmpSDtEOVCVL/z+IDBtSyXyaBsdZqNECKxVZDczDbqqLdeycl/lBfFOljxcBs=",
                "addcode": f"{addcode}",
                "stepcode": "",
                "cjsj": cjsj,
                "isXsd": 1,
            },
        ]
        response = await self.aClient.post(
            "https://pg.eeagd.edu.cn/ks/auth/login.jsmeb",
            json=login_data,
            cookies=cookies,
        )

        logger.debug(f"Login response: {response.json()}")
        res_json = response.json()["result"]

        if res_json["code"] == -1:
            if "验证码" in res_json["msg"]:
                raise VerifyCodeError
            if "未注册" in res_json["msg"]:
                raise NotRegister(f"{res_json['msg']}")
            raise GDGKException(f"{res_json['msg']}")
        logger.success(f"Login success: {res_json}")

    async def entry(
        self,
        username: str,
        task_name: str = "default",
    ):

        cjsj, _, cookies = await self.gen_verifycode_param()
        logger.debug(f"[{task_name}] Get verifyCode parameter: cjsj: {cjsj}")
        pic_data, cookies = await self.get_verifycode_pic(cookies=cookies)
        logger.debug(f"[{task_name}] verifyCode cjsj: {cjsj}")
        addcode = await self.identify_code(pic_data)
        logger.debug(f"[{task_name}] Identify code: {addcode}")
        # addcode = input("请输入验证码: ")

        # may raise exception below
        # try:
        await self.login(username=username, addcode=addcode, cjsj=cjsj, cookies=cookies)
        # except VerifyCodeError as e:
        #     await self.save_pic(pic_data, f"{addcode}")
        #     raise e

    async def run(self, task_name="default"):
        global TOTAL, SUCCESS, VERIFYERROR, HTTPERROR, DETECTED
        while True:
            username = generate_id()
            try:
                TOTAL += 1
                await self.entry(task_name=task_name, username=username)
                continue
            except VerifyCodeError as e:
                VERIFYERROR += 1
                logger.warning(f"[{task_name}] Verify code error: {e}, retry...")
                continue
            except GDGKException as e:
                SUCCESS += 1
                if isinstance(e, NotRegister):
                    # print(f"[{username}] NotRegister: {e}")
                    logger.warning(f"[{task_name}] NotRegister: {e}")
                    continue

                logger.warning(f"[{task_name}] GDGKError: {e}")
                with open(Path(ROOTPATH / "success.txt"), "a") as f:
                    DETECTED += 1
                    f.write(f"{username}\n")
                continue
            except httpx.HTTPError as e:
                HTTPERROR += 1
                logger.error(f"HTTP Error: {e.args}")
                logger.trace(e)
                # raise e
                continue
            finally:
                # need to wait verify code fresh.
                # await asyncio.sleep(random.random())
                pass


def print_progress():
    start_time = time.time()

    async def wapper():
        global TOTAL, SUCCESS, VERIFYERROR, HTTPERROR, DETECTED
        while True:
            infos = f"Total: {TOTAL}, VCE: {VERIFYERROR}, HTTPE: {HTTPERROR}, Success: {SUCCESS}, rate: {SUCCESS/(TOTAL or 1):.2%} Total qps: {TOTAL/((time.time()-start_time) or 1):.2f} Success qps: {SUCCESS/((time.time()-start_time) or 1):.2f} Detected: {DETECTED}"
            if PRINT_FLUSH:
                sys.stdout.write(
                    "\r" + infos,
                )
                sys.stdout.flush()
            else:
                print(infos)

            await asyncio.sleep(1)

    return wapper


@logger.catch()
async def main():
    gk = GDGK()
    async_print_progress = print_progress()
    asyncio.create_task(gk.clear_cjsjs())
    asyncio.create_task(async_print_progress())
    await asyncio.gather(*[gk.run(f"Task-{num}") for num in range(ASYNC_COUNT)])

    # test
    # gk = GDGK()
    # await gk.gen_verifycode_param()
    # pic = await gk.get_verifycode_pic(1714397794710)
    # img = Image.open(pic[0])
    # img.show()
    # print("cookie: ", gk.aClient.cookies)


def display_top(snapshot, key_type="lineno", limit=10):
    import linecache

    print("[ Top 10 ]")
    snapshot = snapshot.filter_traces(
        (
            tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
            tracemalloc.Filter(False, "<unknown>"),
        )
    )
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        print(
            "#%s: %s:%s: %.1f KiB"
            % (index, frame.filename, frame.lineno, stat.size / 1024)
        )
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print("    %s" % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))


if __name__ == "__main__":
    logger.remove()
    import sys

    if DEBUG:
        logger.add(sys.stderr, level="DEBUG")

    try:
        try:
            if DISABLE_UVLOOP:
                raise ImportError
            import uvloop  # type: ignore

            uvloop.run(main())
        except ImportError:
            asyncio.run(main())

        # Note: more details see issue: https://github.com/encode/httpx/issues/914
        # loop = asyncio.get_event_loop()
        # loop.run_until_complete(main())
    except KeyboardInterrupt:
        if DEBUG:
            snapshot = tracemalloc.take_snapshot()
            # top_stats = snapshot.statistics("lineno")
            display_top(snapshot, key_type="lineno", limit=10)

        print("KeyboardInterrupt, exit...")

        print(
            f"Finished: Total: {TOTAL}, Success: {SUCCESS}, rate: {SUCCESS/TOTAL:.2%}"
        )

        logger.warning(f"Total: {TOTAL}, Success: {SUCCESS}, rate: {SUCCESS/TOTAL:.2%}")
        logger.warning("KeyboardInterrupt, exit...")
