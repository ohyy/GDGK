import httpx
import asyncio

PROXY = "http://127.0.0.1:10809"


headers = {
    "Host": "aiexam-yun.pingan.com.cn",
    "xweb_xhr": "1",
    # "X-Jwt-Token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhaWV4YW0udjEiLCJzdWIiOiJiOGExYTdiOWZmNWI0MzY4ODFmMDhmODZjYzBiNDEyMCIsImlhdCI6MTcxNDQ3NzU3MiwiZXhwIjoxNzE0NTYzOTcyLCJ0eXBlIjoicHVibGljIn0.Bjw7Nk8_guNj_lYDRHirEXnkuh80Z0ZSl1u5Y2dlBEQ",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 MicroMessenger/7.0.20.1781(0x6700143B) NetType/WIFI MiniProgramEnv/Windows WindowsWechat/WMPF WindowsWechat(0x6309092b) XWEB/8555",
    "Content-Type": "application/json",
    "Accept": "*/*",
    "Sec-Fetch-Site": "cross-site",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Dest": "empty",
    "Referer": "https://servicewechat.com/wx5b52e4940d09d6ec/135/page-frame.html",
    "Accept-Language": "zh-CN,zh;q=0.9",
}


json_data = {
    "request_id": "9086e28e06e711ef92340123456789ab",
    "info": {
        "plat": "aa",
        "device": "ios",
    },
    "data": {
        "id_score_query": "28",
        "input_param_value": [
            "220604031346",
            "cZQn7U.y8Ma",
        ],
    },
}
aClient = httpx.AsyncClient(proxies=PROXY, headers=headers, verify=False)


async def get_token():
    json_data = {
        # "x-aep-oac-bsid": "6e28e06e711ef92340123456",
        "x-aep-request-id": "9086e28e06e711ef92340123456789ab",
        # "x-aep-user-id": "b8a1a7b9ff5b436881f08f86cc0b4120",
    }
    response = await aClient.post(
        "https://aiexam-yun.pingan.com.cn/v1/auth/acctoken",
        json=json_data,
    )
    # return response.json()
    print(response.text)


async def main():
    # response = await aClient.post(
    #     "https://aiexam-yun.pingan.com.cn/v1/scorequery/query/score",
    #     json=json_data,
    # )
    # print(response.json())
    await get_token()


if __name__ == "__main__":
    asyncio.run(main())
